﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Iserv.Common.BL.DTO;
using Iserv.Common.DL;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL
{
    public interface IVisitBl : IBaseBl<IVisitRepo, Visit>
    {
        Task<Tuple<int, IEnumerable<VisitPagedDto>>> GetForPage(int page, int limit, string term = null);
    }
}