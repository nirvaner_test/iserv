﻿using System;
using System.Collections.Generic;
using System.Text;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL.DTO
{
    public class VisitorSearchDto
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public VisitorSearchDto(Visitor visitor)
        {
            Id = visitor.Id.ToString();
            Name = $"{visitor.Name} {visitor.Surname}";
        }
    }
}
