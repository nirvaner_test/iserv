﻿using Iserv.Common.DL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Iserv.Common.BL.DTO
{
    public class VisitPagedDto
    {
        public Guid Id { get; set; }
        public DateTime VisitDate { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public VisitPagedDto(Visit visit)
        {
            Id = visit.Id;
            VisitDate = visit.VisitDate;
            Name = visit.Visitor.Name;
            Surname = visit.Visitor.Surname;
        }
    }
}
