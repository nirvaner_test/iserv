﻿using Iserv.Common.DL;
using Iserv.Common.DL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iserv.Common.BL.DTO;

namespace Iserv.Common.BL
{
    public class VisitorBl : BaseBl<IVisitorsRepo, Visitor>, IVisitorBl
    {
        public VisitorBl(IVisitorsRepo repo) : base(repo)
        {
        }

        public async Task<Tuple<int, List<Visitor>>> GetForPage(int page, int limit, string term = null)
        {
            return await Repo.GetForPage(page, limit, term);
        }

        public async Task<IEnumerable<VisitorSearchDto>> Search(string term)
        {
            var res = await Repo.Search(term);
            return res.Select(e => new VisitorSearchDto(e));
        }

        public override async Task AddOne(Visitor entity)
        {
            var check = await Repo.CheckNames(entity.Id, entity.Name, entity.Surname);
            if (check) throw new ArgumentException("Already exists");
            await base.AddOne(entity);
        }

        public override async Task ReplaceOne(string id, Visitor entity)
        {
            var check = await Repo.CheckNames(entity.Id, entity.Name, entity.Surname);
            if (check) throw new ArgumentException("Already exists");
            await base.ReplaceOne(id, entity);
        }
    }
}
