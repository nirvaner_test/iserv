﻿using System.Threading.Tasks;
using Iserv.Common.DL;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL
{
    public abstract class BaseBl<TRepo, TEntity> : IBaseBl<TRepo, TEntity>
        where TRepo : IBaseRepo<TEntity>
        where TEntity : BaseEntity
    {
        protected readonly TRepo Repo;

        protected BaseBl(TRepo repo)
        {
            Repo = repo;
        }

        public async Task<TEntity> GetById(string id)
        {
            var entity = await Repo.GetById(id);
            return entity;
        }

        public virtual async Task ReplaceOne(string id, TEntity entity)
        {
            await Repo.ReplaceOne(id, entity);
        }

        public virtual async Task AddOne(TEntity entity)
        {
            await Repo.AddOne(entity);
        }
    }
}
