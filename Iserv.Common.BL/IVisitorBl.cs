﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Iserv.Common.BL.DTO;
using Iserv.Common.DL;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL
{
    public interface IVisitorBl : IBaseBl<IVisitorsRepo, Visitor>
    {
        Task<Tuple<int, List<Visitor>>> GetForPage(int page, int limit, string term = null);
        Task<IEnumerable<VisitorSearchDto>> Search(string term);
    }
}