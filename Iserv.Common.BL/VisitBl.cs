﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iserv.Common.BL.DTO;
using Iserv.Common.DL;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL
{
    public class VisitBl : BaseBl<IVisitRepo, Visit>, IVisitBl
    {
        public VisitBl(IVisitRepo repo) : base(repo)
        {
        }
        public async Task<Tuple<int, IEnumerable<VisitPagedDto>>> GetForPage(int page, int limit, string term = null)
        {
            var res = await Repo.GetForPage(page, limit, term);
            var entities = res.Item2.Select(e => new VisitPagedDto(e));
            return new Tuple<int, IEnumerable<VisitPagedDto>>(res.Item1, entities);
        }
    }
}
