﻿using System.Threading.Tasks;
using Iserv.Common.DL;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.BL
{
    public interface IBaseBl<TRepo, TEntity>
        where TRepo : IBaseRepo<TEntity>
        where TEntity : BaseEntity
    {
        Task AddOne(TEntity entity);
        Task<TEntity> GetById(string id);
        Task ReplaceOne(string id, TEntity entity);
    }
}