﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Iserv.Common.API.Migrations
{
    public partial class VisitorId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_Visitors_VisitorId1",
                table: "Visits");

            migrationBuilder.DropIndex(
                name: "IX_Visits_VisitorId1",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "VisitorId1",
                table: "Visits");

            migrationBuilder.AlterColumn<Guid>(
                name: "VisitorId",
                table: "Visits",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VisitorId",
                table: "Visits",
                column: "VisitorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_Visitors_VisitorId",
                table: "Visits",
                column: "VisitorId",
                principalTable: "Visitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_Visitors_VisitorId",
                table: "Visits");

            migrationBuilder.DropIndex(
                name: "IX_Visits_VisitorId",
                table: "Visits");

            migrationBuilder.AlterColumn<string>(
                name: "VisitorId",
                table: "Visits",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "VisitorId1",
                table: "Visits",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VisitorId1",
                table: "Visits",
                column: "VisitorId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_Visitors_VisitorId1",
                table: "Visits",
                column: "VisitorId1",
                principalTable: "Visitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
