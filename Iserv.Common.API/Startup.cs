﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using Iserv.Common.DL;

namespace Iserv.Common.API
{
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }
        public static IContainer AutofaContainer { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration);
            Log.Logger = logger.CreateLogger();

            services.AddCors(x => x.AddPolicy("AllowAll", new CorsPolicy
            {
                Origins = { "*" },
                Headers = { "*" },
                Methods = { "*" },
                SupportsCredentials = true
            }));

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            }).AddJsonOptions(x =>
            {
                x.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Iserv", Version = "v1" });
            });

            services.AddDbContext<OrgContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SqlServer"),
                b => b.MigrationsAssembly("Iserv.Common.API")));

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterInstance(Configuration);
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule<AutofacConfigModule>();
            AutofaContainer = containerBuilder.Build();
            return AutofaContainer.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            app.UseCors("AllowAll");

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "docs";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
