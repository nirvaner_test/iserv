﻿using System;
using System.Threading.Tasks;
using Iserv.Common.BL;
using Iserv.Common.DL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Iserv.Common.API.Controllers
{
    public class VisitorsController : BaseController
    {
        private readonly IVisitorBl _logic;

        public VisitorsController(IVisitorBl logic)
        {
            _logic = logic;
        }

        [HttpGet]
        [Route("paged")]
        public async Task<IActionResult> Paged([FromQuery] int page, [FromQuery] int limit, [FromQuery] string term)
        {
            try
            {
                if (page < 1) throw new ArgumentException("Page must be greather than 0");
                if (limit < 1) throw new ArgumentException("Limit must be greather than 0");

                var res = await _logic.GetForPage(page, limit, term);
                return Ok(new { count = res.Item1, items = res.Item2 });
            }
            catch (ArgumentException e)
            {
                return Warning(e, page, limit);
            }
            catch (Exception e)
            {
                return Error(e, page, limit);
            }
        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Search([FromQuery] string term)
        {
            try
            {
                var res = await _logic.Search(term);
                return Ok(res);
            }
            catch (ArgumentException e)
            {
                return Warning(e, term);
            }
            catch (Exception e)
            {
                return Error(e, term);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> ById([FromRoute] string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id)) throw new ArgumentException("id required");

                var res = await _logic.GetById(id);
                return Ok(res);
            }
            catch (Exception e)
            {
                if (e is ArgumentException || e is FormatException)
                    return Warning(e, id);
                return Error(e, id);
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Insert([FromBody] Visitor entity)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                await _logic.AddOne(entity);
                return Ok();
            }
            catch (Exception e)
            {
                if (e is ArgumentException || e is FormatException)
                    return Warning(e, entity);
                return Error(e, entity);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] Visitor entity)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                await _logic.ReplaceOne(id, entity);
                return Ok();
            }
            catch (Exception e)
            {
                if (e is ArgumentException || e is FormatException)
                    return Warning(e, entity);
                return Error(e, entity);
            }
        }
    }
}