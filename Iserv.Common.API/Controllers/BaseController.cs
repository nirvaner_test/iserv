﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Iserv.Common.API.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BaseController : Controller
    {
        protected IActionResult Warning(Exception ex, params object[] args)
        {
            var desc = ControllerContext.ActionDescriptor;
            Log.Warning(ex, $"{desc.ControllerName} => {desc.ActionName}", args);
            return BadRequest(new { ex.Message });
        }

        protected IActionResult Error(Exception ex, params object[] args)
        {
            var desc = ControllerContext.ActionDescriptor;
            Log.Error(ex, $"{desc.ControllerName} => {desc.ActionName}", args);
            return StatusCode(500, new { ex.Message });
        }
    }
}