﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Iserv.Common.API
{
    public class ConfigFromEnv
    {
        public void Run(IConfiguration config)
        {
            var env = Environment.GetEnvironmentVariables();
            if (env.Contains("ASPNETCORE_URLS")) config["URLS:COMMON"] = env["ASPNETCORE_URLS"].ToString();
        }
    }
}
