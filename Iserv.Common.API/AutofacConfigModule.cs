﻿using Autofac;

namespace Iserv.Common.API
{
    public class AutofacConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assemblyData = System.Reflection.Assembly
                .Load(new System.Reflection.AssemblyName("Iserv.Common.DL"));
            builder.RegisterAssemblyTypes(assemblyData)
                .Where(t => t.Name.EndsWith("Repo"))
                .AsImplementedInterfaces();

            var assemblyLogic = System.Reflection
                .Assembly.Load(new System.Reflection.AssemblyName("Iserv.Common.BL"));
            builder.RegisterAssemblyTypes(assemblyLogic)
                .Where(t => t.Name.EndsWith("Bl"))
                .AsImplementedInterfaces();

            //builder.RegisterType<Mdb>().SingleInstance();
        }
    }
}
