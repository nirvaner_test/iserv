﻿using System.Threading.Tasks;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.DL
{
    public interface IBaseRepo<TEntity> where TEntity : BaseEntity
    {
        Task AddOne(TEntity entity);
        Task<TEntity> GetById(string id);
        Task ReplaceOne(string id, TEntity entity);
    }
}