﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iserv.Common.DL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Iserv.Common.DL
{
    public class VisitRepo : BaseRepo<Visit>, IVisitRepo
    {
        public VisitRepo(OrgContext context) : base(context)
        {
        }

        public async Task<Tuple<int, List<Visit>>> GetForPage(int page, int limit, string term)
        {
            page--;
            var query = Repo.AsQueryable();
            var countTask = query.CountAsync();
            var entities = await query
                .Where(e => string.IsNullOrWhiteSpace(term) ||
                            e.Visitor.Name.Contains(term) || e.Visitor.Surname.Contains(term))
                .OrderByDescending(e => e.VisitDate)
                .Skip(page * limit)
                .Take(limit)
                .Include(e => e.Visitor)
                .ToListAsync();
            var count = await countTask;
            return new Tuple<int, List<Visit>>(count, entities);
        }
    }
}
