﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.DL
{
    public class OrgContext : DbContext
    {
        public OrgContext(DbContextOptions options) : base(options) { }

        public DbSet<Visitor> Visitors { get; set; }
        public DbSet<Visit> Visits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
