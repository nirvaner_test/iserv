﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.DL
{
    public interface IVisitorsRepo : IBaseRepo<Visitor>
    {
        Task<Tuple<int, List<Visitor>>> GetForPage(int page, int limit, string term);
        Task<bool> CheckNames(Guid id, string name, string surname);
        Task<List<Visitor>> Search(string term);
    }
}