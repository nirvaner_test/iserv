﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Iserv.Common.DL.Entities;

namespace Iserv.Common.DL
{
    public interface IVisitRepo : IBaseRepo<Visit>
    {
        Task<Tuple<int, List<Visit>>> GetForPage(int page, int limit, string term);
    }
}