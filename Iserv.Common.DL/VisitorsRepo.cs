﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iserv.Common.DL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Iserv.Common.DL
{
    public class VisitorsRepo : BaseRepo<Visitor>, IVisitorsRepo
    {
        public VisitorsRepo(OrgContext context) : base(context)
        {
        }

        public async Task<Tuple<int, List<Visitor>>> GetForPage(int page, int limit, string term)
        {
            page--;
            var query = Repo.AsQueryable()
                .Where(e => string.IsNullOrWhiteSpace(term) || e.Name.Contains(term) || e.Surname.Contains(term));
            var countTask = query.CountAsync();
            var entities = await query
                .Skip(page * limit)
                .Take(limit)
                .ToListAsync();
            var count = await countTask;
            return new Tuple<int, List<Visitor>>(count, entities);
        }

        public async Task<List<Visitor>> Search(string term)
        {
            return await Repo.AsQueryable()
                .Where(e => e.IsActive &&
                            (string.IsNullOrWhiteSpace(term) ||
                             e.Name.Contains(term) ||
                             e.Surname.Contains(term)))
                .Take(10)
                .ToListAsync();
        }

        public async Task<bool> CheckNames(Guid id, string name, string surname)
        {
            return await Repo.AnyAsync(e =>
            e.Id != id &&
            e.Name.ToLower() == name.ToLower() &&
            e.Surname.ToLower() == surname.ToLower());
        }
    }
}