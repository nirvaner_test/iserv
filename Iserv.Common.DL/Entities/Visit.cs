﻿using System;

namespace Iserv.Common.DL.Entities
{
    public class Visit : BaseEntity
    {
        public Guid VisitorId { get; set; }

        public DateTime VisitDate { get; set; }

        public virtual Visitor Visitor { get; set; }
    }
}
