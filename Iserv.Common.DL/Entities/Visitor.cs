﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Iserv.Common.DL.Entities
{
    public class Visitor : BaseEntity
    {
        public bool IsActive { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        public DateTime Birthdate { get; set; }

        public SexEnum Sex { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }
    }

    public enum SexEnum
    {
        Male = 0,
        Female = 1
    }
}
