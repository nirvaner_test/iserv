﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Iserv.Common.DL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Iserv.Common.DL
{
    public abstract class BaseRepo<TEntity> : IBaseRepo<TEntity> where TEntity : BaseEntity
    {
        protected readonly OrgContext Context;
        protected readonly DbSet<TEntity> Repo;

        protected BaseRepo(OrgContext context)
        {
            Context = context;
            Repo = Context.Set<TEntity>();
        }

        public async Task<TEntity> GetById(string id)
        {
            var guid = new Guid(id);
            var result = await Repo.Where(e => e.Id == guid).SingleOrDefaultAsync();
            return result;
        }

        public async Task ReplaceOne(string id, TEntity entity)
        {
            var guid = new Guid(id);
            Context.Entry(entity).State = EntityState.Modified;

            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Repo.Any(e => e.Id == guid))
                {
                    throw new ArgumentException("Entity not found");
                }
                throw;
            }
        }

        public async Task AddOne(TEntity entity)
        {
            Repo.Add(entity);
            await Context.SaveChangesAsync();
        }

    }
}
